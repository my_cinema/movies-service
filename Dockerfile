FROM golang:alpine as modules

ADD go.mod go.sum /m/
RUN cd /m && go mod download

FROM golang:latest as builder

COPY --from=modules /go/pkg /go/pkg

#RUN adduser -D -u 10001 myapp
RUN mkdir -p /app
ADD . /app
WORKDIR /app
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -o ./bin/main .

FROM scratch

#COPY --from=builder /etc/passwd /etc/passwd
#USER myapp

COPY --from=builder /app/bin/main /app/main

WORKDIR /app

#FROM golang:alpine
#RUN mkdir /app
#ADD . /app/
#WORKDIR /app
ENV MOVIES_SERVICE_GRPC_ADDR=9092
#RUN go build -o main .
CMD ["/app/main"]
EXPOSE 9092