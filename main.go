package main

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"log"
	pb "moviesmod/api"
	"net"
	"net/http"
	"os"
	"time"
)

func main() {
	addr := os.Getenv("MOVIES_SERVICE_GRPC_ADDR")
	if addr == "" {
		log.Fatal("Unable to retrieve env MOVIES_SERVICE_GRPC_ADDR")
	}
	lis, err := net.Listen("tcp", fmt.Sprintf("%s", "movies-service:"+addr))
	//lis, err := net.Listen("tcp", fmt.Sprintf("%s", "0.0.0.0:"+addr))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe("0.0.0.0:8092", nil)
	}()

	s := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionIdle: 5 * time.Minute,
		}),
	)
	pb.RegisterMovieServiceServer(s, NewMovieService())
	log.Println("Serving GRPC on " + addr)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
