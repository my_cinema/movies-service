package main

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pb "moviesmod/api"
)

type MovieService struct {
	db []*pb.Movie
}

//func Timestamp(t time.Time) *timestamp.Timestamp {
//	return &timestamp.Timestamp{
//		Seconds: t.Unix(),
//		Nanos:   int32(t.Nanosecond()),
//	}
//}

func NewMovieService() pb.MovieServiceServer {
	return &MovieService{
		db: []*pb.Movie{
			{Id: 1, Title: "Бойцовский клуб", Category: pb.MovieCategory_MovieCategory_ACTION},
			{Id: 2, Title: "Крестный отец", Category: pb.MovieCategory_MovieCategory_ACTION},
			{Id: 3, Title: "Криминальное чтиво", Category: pb.MovieCategory_MovieCategory_HORROR},
		},
	}
}

func (s *MovieService) ListMovies(ctx context.Context, req *pb.ListMoviesRequest) (*pb.ListMoviesResponse, error) {
	return &pb.ListMoviesResponse{
		Total:    3,
		PageSize: 3,
		PageNum:  1,
		Movies:   s.db,
	}, nil
}

func (s *MovieService) GetMovie(ctx context.Context, req *pb.GetMovieRequest) (*pb.Movie, error) {
	return nil, nil
}

func (s *MovieService) CreateMovie(ctx context.Context, req *pb.CreateMovieRequest) (*pb.Movie, error) {
	return nil, nil
}

func (s *MovieService) UpdateMovie(ctx context.Context, req *pb.UpdateMovieRequest) (*pb.Movie, error) {
	return nil, nil
}

func (s *MovieService) DeleteMovie(ctx context.Context, req *pb.DeleteMovieRequest) (*empty.Empty, error) {
	return nil, nil
}

func (s *MovieService) RentMovie(ctx context.Context, req *pb.RentMovieRequest) (*empty.Empty, error) {
	return nil, nil
}

func (s *MovieService) ListRents(ctx context.Context, req *pb.ListRentsRequest) (*pb.ListRentsResponse, error) {
	return nil, nil
}

func (s *MovieService) EndRent(ctx context.Context, req *pb.EndRentRequest) (*empty.Empty, error) {
	return nil, nil
}
